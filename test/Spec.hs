{-
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
-}
import Test.Hspec ( hspec
                  , describe
                  , it
                  , shouldBe
                  , shouldSatisfy 
                  )
import Data.Ratio ((%))
import Lib ( abbreviations
           , baseName 
           )

main :: IO ()
main = hspec $ do
    describe "baseName" $ do
        it "returns the predefined base names for specific bases" $ do
            baseName 0   `shouldBe` "nullary"
            baseName 1   `shouldBe` "unary"
            baseName 2   `shouldBe` "binary"
            baseName 3   `shouldBe` "trinary"
            baseName 4   `shouldBe` "quaternary"
            baseName 5   `shouldBe` "quinary"
            baseName 6   `shouldBe` "seximal"
            baseName 7   `shouldBe` "septimal"
            baseName 8   `shouldBe` "octal"
            baseName 9   `shouldBe` "nonary"
            baseName 10  `shouldBe` "decimal"
            baseName 11  `shouldBe` "elevenary"
            baseName 12  `shouldBe` "dozenal"
            baseName 13  `shouldBe` "baker's dozenal"
            baseName 16  `shouldBe` "hex"
            baseName 17  `shouldBe` "suboptimal"
            baseName 20  `shouldBe` "vigesimal"
            baseName 36  `shouldBe` "niftimal"
            baseName 100 `shouldBe` "centesimal"
        
        it "returns the correct base names for bases from 14 to 35" $ do
            baseName 14 `shouldBe` "biseptimal"
            baseName 15 `shouldBe` "triquinary"
            baseName 18 `shouldBe` "triseximal"
            baseName 19 `shouldBe` "untriseximal"
            baseName 21 `shouldBe` "triseptimal"
            baseName 22 `shouldBe` "bielevenary"
            baseName 23 `shouldBe` "unbielevenary"
            baseName 24 `shouldBe` "tetraseximal"
            baseName 25 `shouldBe` "pentaquinary"
            baseName 26 `shouldBe` "biker's dozenal"
            baseName 27 `shouldBe` "trinonary"
            baseName 28 `shouldBe` "tetraseptimal"
            baseName 29 `shouldBe` "untetraseptimal"
            baseName 30 `shouldBe` "pentaseximal"
            baseName 31 `shouldBe` "unpentaseximal"
            baseName 32 `shouldBe` "tetroctal"
            baseName 33 `shouldBe` "trielevenary"
            baseName 34 `shouldBe` "bisuboptimal"
            baseName 35 `shouldBe` "pentaseptimal"
        
        it "returns the correct base names for primes from 19 to 97" $ do
            baseName 19 `shouldBe` "untriseximal"
            baseName 23 `shouldBe` "unbielevenary"
            baseName 29 `shouldBe` "untetraseptimal"
            baseName 31 `shouldBe` "unpentaseximal"
            baseName 37 `shouldBe` "unniftimal"
            baseName 41 `shouldBe` "unpentoctal"
            baseName 43 `shouldBe` "unhexaseptimal"
            baseName 47 `shouldBe` "unbinbielevenary"
            baseName 53 `shouldBe` "untetraker's dozenal"
            baseName 59 `shouldBe` "unbintetraseptimal"
            baseName 61 `shouldBe` "unhexagesimal"
            baseName 67 `shouldBe` "unhexelevenary"
            baseName 71 `shouldBe` "unheptagesimal"
            baseName 73 `shouldBe` "unoctononary"
            baseName 79 `shouldBe` "unhexaker's dozenal"
            baseName 83 `shouldBe` "unbinpentoctal"
            baseName 89 `shouldBe` "unoctelevenary"
            baseName 97 `shouldBe` "unoctodozenal"
        
        it "returns the correct base names for specific negative bases" $ do
            baseName (-0)   `shouldBe` "nullary"
            baseName (-1)   `shouldBe` "negaunary"
            baseName (-2)   `shouldBe` "negabinary"
            baseName (-3)   `shouldBe` "negatrinary"
            baseName (-4)   `shouldBe` "negaquaternary"
            baseName (-5)   `shouldBe` "negaquinary"
            baseName (-6)   `shouldBe` "negaseximal"
            baseName (-7)   `shouldBe` "negaseptimal"
            baseName (-8)   `shouldBe` "negaoctal"
            baseName (-9)   `shouldBe` "neganonary"
            baseName (-10)  `shouldBe` "negadecimal"
            baseName (-11)  `shouldBe` "negaelevenary"
            baseName (-12)  `shouldBe` "negadozenal"
            baseName (-13)  `shouldBe` "negabaker's dozenal"
            baseName (-16)  `shouldBe` "negahex"
            baseName (-17)  `shouldBe` "negasuboptimal"
            baseName (-20)  `shouldBe` "negavigesimal"
            baseName (-36)  `shouldBe` "neganiftimal"
            baseName (-100) `shouldBe` "negacentesimal"
        
        it "returns the correct base names for the reciprocals of specific bases" $ do
            baseName (1 % 2  ) `shouldBe` "votbinary"
            baseName (1 % 3  ) `shouldBe` "vottrinary"
            baseName (1 % 4  ) `shouldBe` "votquaternary"
            baseName (1 % 5  ) `shouldBe` "votquinary"
            baseName (1 % 6  ) `shouldBe` "votseximal"
            baseName (1 % 7  ) `shouldBe` "votseptimal"
            baseName (1 % 8  ) `shouldBe` "votoctal"
            baseName (1 % 9  ) `shouldBe` "votnonary"
            baseName (1 % 10 ) `shouldBe` "votdecimal"
            baseName (1 % 11 ) `shouldBe` "votelevenary"
            baseName (1 % 12 ) `shouldBe` "votdozenal"
            baseName (1 % 13 ) `shouldBe` "votbaker's dozenal"
            baseName (1 % 16 ) `shouldBe` "vothex"
            baseName (1 % 17 ) `shouldBe` "votsuboptimal"
            baseName (1 % 20 ) `shouldBe` "votvigesimal"
            baseName (1 % 36 ) `shouldBe` "votniftimal"
            baseName (1 % 100) `shouldBe` "votcentesimal"
        
        it "returns the correct base names for the doubles of the reciprocals of specific bases" $ do
            baseName (2 % 2  ) `shouldBe` "unary"
            baseName (2 % 3  ) `shouldBe` "bivottrinary"
            baseName (2 % 4  ) `shouldBe` "votbinary"
            baseName (2 % 5  ) `shouldBe` "bivotquinary"
            baseName (2 % 6  ) `shouldBe` "vottrinary"
            baseName (2 % 7  ) `shouldBe` "bivotseptimal"
            baseName (2 % 8  ) `shouldBe` "votquaternary"
            baseName (2 % 9  ) `shouldBe` "bivotnonary"
            baseName (2 % 10 ) `shouldBe` "votquinary"
            baseName (2 % 11 ) `shouldBe` "bivotelevenary"
            baseName (2 % 12 ) `shouldBe` "votseximal"
            baseName (2 % 13 ) `shouldBe` "bivotbaker's dozenal"
            baseName (2 % 16 ) `shouldBe` "votoctal"
            baseName (2 % 17 ) `shouldBe` "bivotsuboptimal"
            baseName (2 % 20 ) `shouldBe` "votdecimal"
            baseName (2 % 36 ) `shouldBe` "vottriseximal"
            baseName (2 % 100) `shouldBe` "vot" ++ baseName 50

    
    describe "abbreviations" $ do
        it "returns abbreviations from 1 to 1296 and ensures all are unique" $ do
            let allDifferent :: (Eq a) => [a] -> Bool
                allDifferent list = case list of
                    []     -> True 
                    (x:xs) -> x `notElem` xs && allDifferent xs
            abbreviations 1296 `shouldSatisfy` allDifferent
        
        it "returns the expected abbreviation for the predefined bases" $ do
            let abbrs = abbreviations 100
            head abbrs  `shouldBe` "UNA"
            abbrs !! 1  `shouldBe` "BIN"
            abbrs !! 2  `shouldBe` "TRI"
            abbrs !! 3  `shouldBe` "QUA"
            abbrs !! 4  `shouldBe` "QUI"
            abbrs !! 5  `shouldBe` "SEX"
            abbrs !! 6  `shouldBe` "SEP"
            abbrs !! 7  `shouldBe` "OCT"
            abbrs !! 8  `shouldBe` "NON"
            abbrs !! 9  `shouldBe` "DEC"
            abbrs !! 10 `shouldBe` "ELE"
            abbrs !! 11 `shouldBe` "DOZ"
            abbrs !! 12 `shouldBe` "BAK"
            abbrs !! 15 `shouldBe` "HEX"
            abbrs !! 16 `shouldBe` "SUB"
            abbrs !! 19 `shouldBe` "VIG"
            abbrs !! 35 `shouldBe` "NIF"
            abbrs !! 99 `shouldBe` "CEN"

