# Changelog for misalian-nomenclature

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to the [Haskell Package Versioning Policy](https://pvp.haskell.org/).

## Unreleased changes

## [0.1.0.0] 2021-07-04

### Added

- Created new project
- Added CLI application that exposes the `baseName` and `abbreviations` function.
- Added basic tests.
