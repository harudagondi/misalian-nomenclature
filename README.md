# misalian-nomenclature

[![Gitlab Build](https://img.shields.io/gitlab/pipeline/harudagondi/misalian-nomenclature/master)](https://gitlab.com/harudagondi/misalian-nomenclature/-/pipelines)

The Misalian Nomenclature is an implementation of *jan Misali*'s system of naming bases without relying on decimal centrism. It is based on *jan Misali*'s [video](https://youtu.be/7OEF3JD-jYo) and their earlier post on their [website](https://www.seximal.net/names-of-other-bases).

The current implementation only have implementations for all integers. It does not have implementations for reciprocal, fractional, irrational, and transcendental bases. Additionally, the abbreviation implementation does not follow *jan Misali*'s specification simply because I do not understand it myself. Therefore, this implementations uses a permutation/combination function that is described in the code.

Fun fact: The original implementation was written in Rust before refactoring it more elegantly in Haskell.

## Table of Contents

[[_TOC_]]

## Installation and Usage

Currently I don't know how to distribute Haskell programs. To build this project, use `stack`:

```zsh
> stack build
> stack exec misalian-nomenclature-exe -- -h
misalian-nomenclature - generate base names

Usage: misalian-nomenclature-exe ((-b|--base-name BASE) | (-l|--abbrevs UP_TO) |
                                   (-a|--abbrev-of BASE))
  Generate base names and abbreviations using jan Misali's nomenclature of bases

Available options:
  -b,--base-name BASE      Generate name for base input.
  -l,--abbrevs UP_TO       Generate abbreviations from 1 up to n.
  -a,--abbrev-of BASE      Generate abbreviation for n.
  -h,--help                Show this help text
```

## Features

### Base Name

Generate a name for a specific base.

```zsh
> stack exec misalian-nomenclature-exe -- -b 1
unary
> stack exec misalian-nomenclature-exe -- --base-name 10
decimal
```

### Abbreviations from one to n

Generate all abbreviations from base 1 to base n.

```zsh
> stack exec misalian-nomenclature-exe -- --abbrevs=100
["UNA","BIN","TRI","QUA","QUI","SEX","SEP","OCT","NON","DEC","ELE","DOZ","BAK","BIS","TIR","HEX","SUB","IRT","UNT","VIG","ITR","BIE","UNB","TET","PEN","BIK","RIT","TTE","UTN","PNE","UNP","ETT","RTI","BSI","NEP","NIF","UNN","BNI","TRK","NPE","UPN","HXE","UNH","TER","ENP","NIB","UBN","XEH","HEP","EPN","TRS","TRE","TNU","XHE","PET","HPE","TRN","NBI","BNU","EXH","UHN","INB","PEH","OTC","PTE","EHX","HNU","RET","TNR","PHE","HUN","TCO","UNO","IBN","TEP","RTE","EPH","HEA","NHU","TOC","ENN","BIP","BUN","EHP","TPE","BIH","NRT","CTO","UON","NNE","HET","ERT","NTR","BIB","ETP","COT","ONU","HTE","NEN","CEN"]
```

### Abbreviations of base

Generate an abbreviation for base n.

```zsh
> stack exec misalian-nomenclature-exe -- --abbrev-of=99
NEN
```

## Roadmap

This current implementation is feature-incomplete. The following will be implemented in the future.

- [x] Base Name
- [x] Abbreviations
- [ ] Fractional Bases
- [ ] Irrational Bases
- [ ] Output base names and abbreviations to file

## Notes

### Abbreviation Algorithm

Here is the documentation of the `abbreviate` function:

```haskell
-- | Generates an abbreviation of a base name.
-- It checks if that name isn't used previously.
-- The abbreviation is determined through the following process:
-- 1. Uppercase the base name and remove all non-alphabetic characters
-- 2. From the new name, generate all possible three letter permutations
-- 3. Find the first abbreviation that isn't generated previously
-- 4. If it isn't, return the abbreviation
-- 5. Otherwise, restart from step 2, but with an additional letter
-- of permutations
```

## License

This project is licensed under the [Mozilla Public License (version 2.0)](LICENSE).
