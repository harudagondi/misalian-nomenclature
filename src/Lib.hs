{-  
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
-}
{-|
Module      : Lib
Description : Implementation of the Misalian Nomenclature of Bases in Haskell
Copyright   : (c) Gio Genre De Asis, 2021
License     : MPL-2.0
Maintainer  : giogdeasis@gmail.com
Stability   : experimental

Please see the README on Gitlab at
<https://gitlab.com/harudagondi/misalian-nomenclature>
-}
module Lib
    ( baseName
    , abbreviations
    ) where

import Data.Ratio ( numerator, denominator, (%) )
import Data.List ( find, permutations )
import Data.Char ( isAlpha, toUpper )

-- | Check if a certain number is prime
isPrime :: (Integral a) => a -> Bool
isPrime k = (k > 1) && null [ x | x <- [2..k - 1], k `mod` x == 0]

-- | Check if a character is a vowel. Does not include 'y'.
isVowel :: Char -> Bool
isVowel c = c `elem` "aeiou"

-- | Generates all factors of a number, and return the middle two.
--
-- Examples:
--
-- >>> factorsOf 12
-- (3,4)
-- 
-- >>> factorsOf 81
-- (9,9)
factorsOf :: (Integral a) => a -> (a, a)
factorsOf n =
    let fs = [i | i <- [1..n], n `mod` i == 0]
        middleIndex = if even $ length fs 
            then length fs `div` 2
            else (length fs - 1) `div` 2
    in if even $ length fs
        then (fs !! (middleIndex - 1), fs !! middleIndex)
        else (fs !! middleIndex, fs !! middleIndex)

-- | Generates permutations of a list choosing n elements.
choose :: Int -> [a] -> [[a]]
choose n list = concatMap permutations $ choose' list [] where
    choose' []     r = [r | length r == n]
    choose' (x:xs) r 
        | length r == n = [r]
        | otherwise     = choose' xs (x:r) 
                        ++ choose' xs r

-- | Generates a name for a specific base.
-- 
-- Examples:
--
-- >>> baseName 26
-- "biker's dozenal"
--
-- >>> baseName 64
-- "octoctal"
--
-- >>> import Data.Ratio
-- >>> baseName $ 5 % 13
-- "pentavotbaker's dozenal"
--
-- >>> baseName $ 99 % 100
-- "ennalevavotcentesimal"
baseName :: Rational -> String
baseName n = if isNegative
        then "nega" ++ name
        else name
    where 
        isNegative = n < 0
        isFraction = denominator n /= 1
        name = if isFraction
            then fracBaseName n
            else rootBaseName (abs $ numerator n) True

-- | Generates a name for a fractional base
-- 
-- Examples:
--
-- >>> import Data.Ratio
-- >>> fracBaseName $ 1 % 2
-- "votbinary"
--
-- >>> fracBaseName $ 1 % 3
-- "vottrinary"
--
-- >>> fracBaseName $ 2 % 3
-- "bivottrinary"
fracBaseName :: Rational -> String
fracBaseName f = num ++ "vot" ++ den where
    num = if numer == 1
        then ""
        else prefix numer where
            numer = numerator f
    den = baseName $ denominator f % 1

-- | Helper function for generating base names.
rootBaseName :: (Integral a) => a -> Bool -> String
rootBaseName 0 _      = "nullary"
rootBaseName 1 _      = "unary"
rootBaseName 2 _      = "binary"
rootBaseName 3 _      = "trinary"
rootBaseName 4 _      = "quaternary"
rootBaseName 5 _      = "quinary"
rootBaseName 6 _      = "seximal"
rootBaseName 7 _      = "septimal"
rootBaseName 8 _      = "octal"
rootBaseName 9 _      = "nonary"
rootBaseName 10 True  = "decimal"
rootBaseName 10 False = "gesimal"
rootBaseName 11 _     = "elevenary"
rootBaseName 12 _     = "dozenal"
rootBaseName 13 True  = "baker's dozenal"
rootBaseName 13 False = "ker's dozenal"
rootBaseName 16 _     = "hex"
rootBaseName 17 _     = "suboptimal"
rootBaseName 20 _     = "vigesimal"
rootBaseName 36 _     = "niftimal"
rootBaseName 100 _    = "centesimal"
rootBaseName n _      = 
    if isPrime n
    then concatenate "un" $ rootBaseName (n - 1) False
    else
        let factors = factorsOf n
        in concatenate (prefix $ fst factors) (rootBaseName (snd factors) False)

-- | Generates a prefix for a specific factor
prefix :: (Integral a)
        => a -- ^ the factor to be converted as a prefix
        -> String
prefix 2   = "bi"
prefix 3   = "tri"
prefix 4   = "tetra"
prefix 5   = "penta"
prefix 6   = "hexa"
prefix 7   = "hepta"
prefix 8   = "octo"
prefix 9   = "enna"
prefix 10  = "deca"
prefix 11  = "leva"
prefix 12  = "doza"
prefix 13  = "baker"
prefix 16  = "tesser"
prefix 17  = "mal"
prefix 20  = "icosi"
prefix 36  = "feta"
prefix 100 = "hecto"
prefix n   = 
    if isPrime n
    then "hen" ++ prefix (n - 1) ++ "sna"
    else
        let factors = factorsOf n
        in concatenate (prefix $ fst factors) (prefix $ snd factors)

-- | Concatenate two string, with special rules for vowel affixes.
-- If the first string ends with 'a' or 'o' and the second string
-- starts with a vowel, remove the 'a' or 'o'.
-- If the first string ends with 'i' and the second ends with 'i' or 'u',
-- remove the 'i' or 'u' in the second string.
-- Otherwise, concatenate normally.
--
-- Examples:
--
-- >>> concatenate "octo" "octal"
-- "octoctal"
--
-- >>> concatenate "bi" "un"
-- "bin"
--
-- >>> concatenate "bi" "septimal"
-- "biseptimal"
concatenate :: String -> String -> String
concatenate s1 s2
    | (last s1 == 'a' || last s1 == 'o') && isVowel (head s2)
    = init s1 ++ s2
    | last s1 == 'i' && (head s2 == 'i' || head s2 == 'u')
    = s1 ++ tail s2
    | otherwise = s1 ++ s2

-- | Generates all abbreviations from 1 to n.
-- All bases to be abbreviated are integers.
-- To get a specific abbreviation, do `abbreviations x !! (x - 1)`,
-- where `x` is the base you want the abbreviation of.
--
-- Examples:
--
-- >>> abbreviations 10
-- ["UNA","BIN","TRI","QUA","QUI","SEX","SEP","OCT","NON","DEC"]
--
-- >>> abbreviations 15 !! (15 - 1)
-- "TIR"
abbreviations :: (Integral a) => a -> [String]
abbreviations n = 
    let upper x = map toUpper $ filter isAlpha $ baseName x
        abbreviations' :: (Integral a) => a -> [String] -> [String]
        abbreviations' 0 abbrs = abbrs
        abbreviations' m abbrs = newAbbr : prevAbbrs where
            prevAbbrs = abbreviations' (m - 1) abbrs
            newAbbr = abbreviate prevAbbrs (upper $ toRational m) 3
    in reverse $ map reverse $ abbreviations' n []

-- | Generates an abbreviation of a base name.
-- It checks if that name isn't used previously.
-- The abbreviation is determined through the following process:
-- 1. Uppercase the base name and remove all non-alphabetic characters
-- 2. From the new name, generate all possible three letter permutations
-- 3. Find the first abbreviation that isn't generated previously
-- 4. If it isn't, return the abbreviation
-- 5. Otherwise, restart from step 2, but with an additional letter
-- of permutations
abbreviate :: (Integral a) => [String] -> String -> a -> String
abbreviate abbrs nameOfBase noOfLetters =
    let permuts = choose (fromIntegral noOfLetters) nameOfBase
        currAbbr = find (`notElem` abbrs) permuts
    in case currAbbr of
        Just abbr -> abbr
        Nothing -> abbreviate abbrs nameOfBase (noOfLetters + 1)
