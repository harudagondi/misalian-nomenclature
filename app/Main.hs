{-  
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
-}
module Main where

import Options.Applicative
    ( (<**>)
    , auto
    , fullDesc
    , header
    , help
    , info
    , long
    , metavar
    , option
    , progDesc
    , short
    , showDefault
    , value
    , execParser
    , helper
    , Alternative((<|>))
    , Parser
    )
import System.IO ()
import Data.Semigroup ((<>))
import Data.Ratio ( (%) )
import Lib ( baseName
           , abbreviations 
           ) 

main :: IO ()
main = cli =<< execParser opts where
    opts = info (opts' <**> helper)
        ( fullDesc 
       <> progDesc "Generate base names and abbreviations using jan Misali's nomenclature of bases"
       <> header "misalian-nomenclature - generate base names")

data Options = 
    OptionsNameOf   Integer |
    OptionsAbbrevs  Integer |
    OptionsAbbrevOf Int 

cli :: Options -> IO ()
cli (OptionsNameOf base) = putStrLn $ baseName $ base % 1 
cli (OptionsAbbrevs upto) = print $ abbreviations upto 
cli (OptionsAbbrevOf base) = putStrLn $ abbreviations base !! (base - 1)

opts' :: Parser Options
opts' = nameOf <|> abbrevs <|> abbrevOf

nameOf :: Parser Options
nameOf = OptionsNameOf <$> option auto
    ( long "base-name"
   <> short 'b'
   <> metavar "BASE"
   <> help "Generate name for base input." )

abbrevs :: Parser Options 
abbrevs = OptionsAbbrevs <$> option auto
    ( long "abbrevs"
   <> short 'l'
   <> metavar "UP_TO"
   <> help "Generate abbreviations from 1 up to n." )

abbrevOf :: Parser Options 
abbrevOf = OptionsAbbrevOf <$> option auto
    ( long "abbrev-of"
   <> short 'a'
   <> metavar "BASE"
   <> help "Generate abbreviation for n." )
